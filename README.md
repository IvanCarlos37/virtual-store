# Nome do Seu Projeto
O **Farmax** é uma aplicação dedicada a facilitar a experiência de compra de medicamentos, proporcionando aos clientes uma maneira mais eficiente e personalizada de gerenciar sua saúde.

- Facilitar a compra de medicamentos de forma intuitiva.
- Oferecer acompanhamento de saúde personalizado aos clientes.

## Requisitos

- Flutter: 3.10.4
- Dart: 3.0.3
## - Rodar no ambiente mobile

Certifique-se de ter o Flutter e o Dart instalados em sua máquina.

## Como Rodar o Projeto
```bash
1. Clone o repositório:

git clone https://gitlab.com/IvanCarlos37/virtual-store.git

2. Ir no repositorio do projeto:

cd caminho_da_pasta_do_projeto

3. Rodar o projeto 
Flutter run
