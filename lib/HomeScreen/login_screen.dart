import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:virtual_store/HomeScreen/sign_up_screen.dart';
import 'package:virtual_store/Models/user_model.dart';

import '../widgets/bottom_menu.dart';
import 'my_profile_screen.dart';

class LoginScreen extends StatefulWidget {
  final PageController pagecontroller;

  const LoginScreen({super.key, required this.pagecontroller});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final passcontroller = TextEditingController();

  final emailcontroller = TextEditingController();

  final bool isLoading = false;

  final formkey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        shape: Border.all(width: 0.5),
        shadowColor: Colors.grey,
        toolbarHeight: 100.0,
        elevation: 0.0,
        flexibleSpace: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24.0),
          child: Row(
            children: [
              SizedBox(
                  child: Image.asset(
                'assets/img/logo.png',
              )),
              const Spacer(),
              const Icon(
                Icons.search,
                size: 25,
                color: Colors.grey,
              ),
              const SizedBox(
                width: 10,
              ),
              const Icon(
                Icons.shopping_cart_outlined,
                size: 25,
                color: Colors.grey,
              ),
            ],
          ),
        ),
        backgroundColor: Colors.transparent,
      ),
      body: Stack(children: [
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: ScopedModelDescendant<UsersModel>(
              builder: (context, child, model) {
                if (model.isLoading) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                } else {
                  return !model.isLogin()
                      ? Form(
                          key: formkey,
                          child: ListView(
                            children: [
                              const SizedBox(
                                height: 90,
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: TextFormField(
                                  controller: emailcontroller,
                                  decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.grey[200],
                                      hintText: "Email",
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(20.0))),
                                  keyboardType: TextInputType.emailAddress,
                                  validator: (text) {
                                    if (text!.isEmpty || !text.contains("@")) {
                                      return "Email invalido";
                                    } else {
                                      return null;
                                    }
                                  },
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: TextFormField(
                                  controller: passcontroller,
                                  decoration: InputDecoration(
                                      hintText: "Palvra-Passe",
                                      border: OutlineInputBorder(
                                          borderSide: const BorderSide(
                                              width: 0.1, color: Colors.grey),
                                          borderRadius:
                                              BorderRadius.circular(20.0)),
                                      filled: true,
                                      fillColor: Colors.grey[200]),
                                  obscureText: true,
                                  validator: (text) {
                                    if (text!.isEmpty || text.length < 6) {
                                      return "Palavra-Passe Invalida";
                                    } else {
                                      return null;
                                    }
                                  },
                                ),
                              ),
                              Row(
                                children: [
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: TextButton(
                                      child: const Text(
                                        "Esqueci minha Palavra-Passe",
                                        style: TextStyle(color: Colors.green),
                                      ),
                                      onPressed: () {
                                        if (emailcontroller.text.isEmpty) {
                                          var snackbar = const SnackBar(
                                            content: Text("Insira o E-mail"),
                                            duration: Duration(seconds: 3),
                                            backgroundColor: Colors.yellow,
                                          );
                                          ScaffoldMessenger.of(context)
                                              .showSnackBar(snackbar);
                                        } else {
                                          model.recoverPass(
                                              emailcontroller.text);
                                          var snackbar = SnackBar(
                                            elevation: 20,
                                            content: const Text(
                                                "Confira o seu email"),
                                            duration:
                                                const Duration(seconds: 3),
                                            backgroundColor:
                                                Theme.of(context).primaryColor,
                                          );
                                          ScaffoldMessenger.of(context)
                                              .showSnackBar(snackbar);
                                        }
                                      },
                                    ),
                                  ),
                                  const Spacer(),
                                  Align(
                                    alignment: Alignment.centerRight,
                                    child: TextButton(
                                      child: const Text(
                                        "Registe-se agora",
                                        style: TextStyle(
                                            color: Colors.green,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 17),
                                      ),
                                      onPressed: () => Navigator.of(context)
                                          .push(MaterialPageRoute(
                                              builder: (context) =>
                                                  SignUpScreen(
                                                      pagecontroller: widget
                                                          .pagecontroller))),
                                    ),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: SizedBox(
                                  height: 44,
                                  child: ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                        backgroundColor: Colors.green,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20.0))),
                                    onPressed: () {
                                      if (formkey.currentState!.validate()) {}
                                      model.sigin(
                                          email: emailcontroller.text,
                                          pass: passcontroller.text,
                                          onsucess: _onsucess,
                                          onfailled: _onfailled);
                                    },
                                    child: const Text(
                                      "Entrar",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white,
                                          fontSize: 18),
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      color: Colors.grey,
                                      width: 1.0,
                                    ),
                                  ),
                                  child: TextButton(
                                    onPressed: () {
                                      model.googleSigin();
                                      Future.delayed(const Duration(seconds: 2));
                                     widget.pagecontroller.jumpToPage(0);
                                    },
                                    style: TextButton.styleFrom(
                                      backgroundColor: Colors.white,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(0),
                                      ),
                                    ),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          flex: 2,
                                          child: Image.network(
                                            'https://t.ctcdn.com.br/P7-_JvQTS4U7-if6zHyXjyMiNQ8=/400x400/smart/i606944.png',
                                            height: 35,
                                          ),
                                        ),
                                        const SizedBox(width: 10),
                                        Expanded(
                                          flex: 10,
                                          child: Text(
                                            "Entrar com o Google",
                                            style: TextStyle(
                                                color: Colors.grey[600],
                                                fontSize: 15),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ))
                      : const MyProfileScreen();
                }
              },
            )),
        BottonMenu(pagecontroller: widget.pagecontroller)
      ]),
    );
  }

  _onsucess() {
    widget.pagecontroller.jumpToPage(0);
  }

  _onfailled(String erro) {
    var snackBar = SnackBar(
      content: Text(erro),
      duration: const Duration(seconds: 2),
      backgroundColor: Colors.red,
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
