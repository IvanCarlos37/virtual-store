import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:virtual_store/Data/product_data.dart';

class Productscreen extends StatefulWidget {
  const Productscreen({super.key, required this.product});
  final ProductData product;

  @override
  State<Productscreen> createState() => _ProductscreenState();
}

class _ProductscreenState extends State<Productscreen> {
  @override
  Widget build(BuildContext context) {
    final Color cor = Theme.of(context).primaryColor;
    return Scaffold(
    
      appBar: AppBar(
          backgroundColor: cor,
        title: Text(widget.product.title.toString()),
      ),
      body: ListView(children: [
        AspectRatio(
          aspectRatio: 0.9,
          child: CarouselSlider(
            items: widget.product.image?.map((url) {
              return Image.network(
                url,
                fit: BoxFit.cover,
              );
            }).toList(),
            options: CarouselOptions(
              enableInfiniteScroll: false,
              viewportFraction: 1,
              height: 350,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Text(
                widget.product.title ?? " ",
                style:
                    const TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                maxLines: 3,
              ),
              Text(
                "${widget.product.price?.toStringAsFixed(2) ?? ""} CVE",
                style: TextStyle(
                    fontSize: 22.0, fontWeight: FontWeight.bold, color: cor),
              ),
              const SizedBox(
                height: 16.0,
              ),
              const Text(
                "Tamanho",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
              ),
              SizedBox(
                height: 34.0,
                child: GridView(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 1,
                      mainAxisSpacing: 8.0,
                      childAspectRatio: 0.5),
                  scrollDirection: Axis.horizontal,
                  padding: const EdgeInsets.symmetric(vertical: 4.0),
                  children: [
                    Container(
                      width: 50.0,
                      decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(4.0)),
                          border: Border.all(
                              width: 1.5,
                              color: const Color.fromARGB(255, 130, 129, 129))),
                      alignment: Alignment.center,
                      child: const Text("500g"),
                    ),
                    Container(
                      width: 50.0,
                      decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(4.0)),
                          border: Border.all(
                              width: 1.5,
                              color: const Color.fromARGB(255, 130, 129, 129))),
                      alignment: Alignment.center,
                      child: const Text("1000g"),
                    )
                  ],
                ),
              ),
              const SizedBox(
                height: 16.0,
              ),
              Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.green, width: 1.5),
                  borderRadius: BorderRadius.circular(20.0),
                ),
                height: 44,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.circular(20.0), // Raio da borda
                    ),
                    backgroundColor: Colors.white
                        .withOpacity(0.9), // Cor do botão transparente
                  ),
                  onPressed: () {},
                  child: const Text(
                    "ADICIONAR AO CARINHO",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        color: Colors.green),
                  ),
                ),
              ),
              const SizedBox(
                height: 16.0,
              ),
              SizedBox(
                height: 44,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      backgroundColor: Colors.green),
                  onPressed: () {},
                  child: const Text(
                    "COMPRAR AGORA",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        color: Colors.white),
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              const Text(
                "Descrição do Produto",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
              ),
              const SizedBox(
                height: 10,
              ),
              const Text(
                "Produto de alta qualidade rrfenjnejbhurb 4b4rb 4 b4rh 4rh 4rb4rbhr  r4 r4yb4rh br4fenriurnuirnirnurn  b h r4",
                style: TextStyle(fontSize: 16.0),
              )
            ],
          ),
        )
      ]),
    );
  }
}
