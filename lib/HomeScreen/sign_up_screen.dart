import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:virtual_store/Models/user_model.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({super.key, required this.pagecontroller});
  final PageController pagecontroller;
  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final formkey = GlobalKey<FormState>();

  final namecontroller = TextEditingController();
  String error = "";
  final passcontroller = TextEditingController();

  final emailcontroller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: const Text(
          "Criar conta",
        ),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 50.0),
        child: ScopedModelDescendant<UsersModel>(
          builder: (context, child, model) {
            if (model.isLoading) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return Form(
                  key: formkey,
                  child: ListView(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          controller: namecontroller,
                          decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.grey[200],
                              hintText: "Nome Completo",
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20.0))),
                          keyboardType: TextInputType.emailAddress,
                          validator: (text) {
                            if (text!.isEmpty) {
                              return "Insira o nome";
                            } else {
                              return null;
                            }
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          controller: emailcontroller,
                          decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.grey[200],
                              hintText: "Email",
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20.0))),
                          keyboardType: TextInputType.emailAddress,
                          validator: (text) {
                            if (text!.isEmpty || !text.contains("@")) {
                              return "Email Inválido";
                            } else {
                              return null;
                            }
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          controller: passcontroller,
                          decoration: InputDecoration(
                              hintText: "Palvra-Passe",
                              border: OutlineInputBorder(
                                  borderSide: const BorderSide(
                                      width: 0.1, color: Colors.grey),
                                  borderRadius: BorderRadius.circular(20.0)),
                              filled: true,
                              fillColor: Colors.grey[200]),
                          obscureText: true,
                          validator: (text) {
                            if (text!.isEmpty || text.length < 6) {
                              return "Palavra-Passe Invalida";
                            } else {
                              return null;
                            }
                          },
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SizedBox(
                          height: 50,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.green,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20.0))),
                            onPressed: () {
                              Map<String, dynamic> userData = {
                                "email": emailcontroller.text,
                                "name": namecontroller.text
                              };
                              if (formkey.currentState!.validate()) {}
                              model.signup(
                                  userdata: userData,
                                  pass: passcontroller.text,
                                  onsucess: _onsucess,
                                  onfailled: _onfailled);
                            },
                            child: const Text(
                              "Criar uma conta",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                  fontSize: 18),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ));
            }
          },
        ),
      ),
    );
  }

  void _onsucess() {
  
    var snackBar = SnackBar(
      content: const Text('Usuário criado com Sucesso'),
      backgroundColor: Theme.of(context).primaryColor,
      duration: const Duration(seconds: 3),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
    Future.delayed(const Duration(seconds: 2))
        .then((value) => Navigator.of(context).pop());
    widget.pagecontroller.jumpToPage(0);
  }

  _onfailled(String erro) {
    var snackBar = SnackBar(
      content: Text(erro),
      backgroundColor: Colors.red,
      duration: const Duration(seconds: 5),
    );
     ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
