import 'package:flutter/material.dart';
import 'package:virtual_store/widgets/maps_menu.dart';

import '../Tabs/home_tab.dart';
import '../Tabs/products_tab.dart';
import 'login_screen.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({super.key});
  final PageController pagecontroller = PageController();
  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: pagecontroller,
      physics: const NeverScrollableScrollPhysics(),
      children: [
        Scaffold(
          body: HomeTab(
            pagecontroller: pagecontroller,
          ),
        ),
         LoginScreen(
          pagecontroller: pagecontroller,
        ),
       
       
        Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.green,
            title: const Text(
              "Categorias",
              style: TextStyle(color: Colors.white),
            ),
            centerTitle: true,
          ),
          body: ProductsTab(
            pagecontroller: pagecontroller,
          ),
        ),
        
     const Scaffold(
      
      body: MapScrean())
      ],
    );
  }
}
