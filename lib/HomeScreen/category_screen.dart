import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:virtual_store/Data/product_data.dart';

import '../Tile/product_tile.dart';

class CategoryScreen extends StatelessWidget {
  const CategoryScreen({super.key, required this.snapshot});
  final DocumentSnapshot snapshot;
  @override
  Widget build(BuildContext context) {
    var data = snapshot.data() as Map<String, dynamic>;

    return DefaultTabController(
        length: 2,
        child: Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.green,
              title: Text(data["title"]),
            ),
            body: FutureBuilder<QuerySnapshot>(
                future: FirebaseFirestore.instance
                    .collection("Produtos")
                    .doc(snapshot.id)
                    .collection("items")
                    .get(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  } else {
                    return GridView.builder(
                        padding: const EdgeInsets.all(4.0),
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2,
                                mainAxisSpacing: 4,
                                crossAxisSpacing: 4,
                                childAspectRatio: 0.65),
                        itemCount: snapshot.data?.docs.length ?? 0,
                        itemBuilder: (context, index) {
                          return ProductTile(
                              type: "gride",
                              product: ProductData.fromdocument(
                                  snapshot.data!.docs[index]));
                        });
                  }
                })));
  }
}
