import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../Models/user_model.dart';

class MyProfileScreen extends StatelessWidget {
  const MyProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<UsersModel>(builder: (context, child, model) {
      return ListView(
        children: [
          const ListTile(
            title: Text("Meus Pedidos"),
            leading: Icon(
              Icons.shopping_bag_outlined,
              size: 26,
            ),
          ),
          const Divider(
            color: Colors.grey,
          ),
          ListTile(
            title: const Text(
              "Categorias",
              style: TextStyle(
                fontSize: 16,
                color: Colors.white
              ),
            ),
            leading: const Icon(
              Icons.category_outlined,
              size: 26,
            ),
            onTap: () {},
          ),
          const Divider(
            color: Colors.grey,
          ),
          ListTile(
            title: const Text(
              "Terminar a sessão",
              style: TextStyle(
                fontSize: 16,
              ),
            ),
            leading: const Icon(
              Icons.logout_outlined,
              size: 26,
            ),
            onTap: () {
              model.signout();
            },
          ),
        ],
      );
    });
  }
}
