import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class ResultCard extends StatefulWidget {
  final DocumentSnapshot snapshot;
  const ResultCard({super.key, required this.snapshot});

  @override
  State<ResultCard> createState() => _ResultCardState();
}

class _ResultCardState extends State<ResultCard> {
  bool click = false;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        
          width: 230,
          decoration: BoxDecoration(
          
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.2),
                  spreadRadius: 1.5,
                  blurRadius: 7,
                  offset: const Offset(0, 7),
                ),
              ],
              color: Colors.white,
              border: Border.all(width: 0.2, color: Colors.grey),
              borderRadius: BorderRadius.circular(10.0)),
          child: Column(
            children: [
              Flexible(
                  flex: 2,
                  child: SizedBox(
                    height: 45,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 16.0, right: 16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                click = !click;
                              
                              });
                            },
                            child: click == false
                                ? Icon(
                                    Icons.shopping_cart_outlined,
                                    size: 32,
                                    color: Colors.grey[700],
                                  )
                                : Icon(
                                    Icons.shopping_cart_rounded,
                                    size: 30,
                                    color: Colors.grey[700],
                                  ),
                          ),
                          /*  const SizedBox(width: 15),
                         Icon(
                            Icons.heart_broken_rounded,
                            size: 30,
                            color: Colors.grey[700],
                          ),*/
                        ],
                      ),
                    ),
                  )),
              Flexible(
                flex: 7,
                child: Padding(
                  padding: const EdgeInsets.only(top: 30.0),
                  child: Image.network(
                    widget.snapshot["image"][0],
                    height: 150,
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Flexible(
                flex: 4,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 30.0),
                      child: Text(
                        "Stop Piolhos Loção Anti-Parasitária Cabelos Longos 100ml c/ Pente",
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            color: Colors.grey[700]),
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Text(
                      "${widget.snapshot["price"]} CVE",
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          color: Colors.green[600],
                          fontSize: 15),
                    ),
                  ],
                ),
              )
            ],
          )),
    );
  }
}
