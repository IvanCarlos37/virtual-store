import 'package:flutter/material.dart';

import '../Tile/drawer_tile.dart';

class CustonDrawer extends StatelessWidget {
  const CustonDrawer({super.key, required this.pagecontroller});
  final PageController pagecontroller;
  Widget buidDrawerBack() => Container(
        decoration: const BoxDecoration(
            gradient: LinearGradient(
                colors: [Color.fromARGB(255, 199, 218, 234), Colors.white],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter)),
      );
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Stack(children: [
        buidDrawerBack(),
        ListView(
          padding: const EdgeInsets.only(left: 32.0, top: 16.0),
          children: [
            Container(
              height: 170,
              margin: const EdgeInsets.only(bottom: 8.0),
              padding: const EdgeInsets.fromLTRB(0.0, 16.0, 16.0, 8.0),
              child: Stack(
                children: [
                  const Positioned(
                      top: 8.0,
                      left: 0.0,
                      child: Text(
                        "Titulo \n Flutter",
                        style: TextStyle(
                            fontSize: 34, fontWeight: FontWeight.bold),
                      )),
                  Positioned(
                    left: 0.0,
                    bottom: 0.0,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          "Olá,",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                        GestureDetector(
                          child: Text(
                            "Entre-se ou Registre-se >",
                            style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontWeight: FontWeight.bold),
                          ),
                          onTap: () {},
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            const Divider(),
            DrawerTile(
              icon: Icons.home,
              text: "Inicio",
              controller: pagecontroller,
              page: 0,
            ),
            DrawerTile(
              icon: Icons.list,
              text: "Produto",
              controller: pagecontroller,
              page: 1,
            ),
            DrawerTile(
              icon: Icons.location_on_outlined,
              text: "Encontre uma loja",
              controller: pagecontroller,
              page: 2,
            ),
            DrawerTile(
              icon: Icons.playlist_add_check,
              text: "Meus Pedidos",
              controller: pagecontroller,
              page: 3,
            )
          ],
        )
      ]),
    );
  }
}
