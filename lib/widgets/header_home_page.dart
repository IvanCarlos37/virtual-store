import 'package:flutter/material.dart';

class HeaderHomePage extends StatelessWidget {
  HeaderHomePage({super.key});
  final TextEditingController textController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        SizedBox(
          width: 100,
          height: 50,
          child: TextField(
            controller: textController,
            decoration: const InputDecoration(
              labelText: 'Pesquisar no Farmax',
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20.0))),
            ),
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        Row(
          children: [
            menuSecondary("Categorias"),
            const SizedBox(
              width: 10,
            ),
            menuSecondary("Vendas"),
          ],
        )
      ],
    );
  }

  Widget menuSecondary(String title) {
    return Container(
      decoration: BoxDecoration(
          border: Border.all(color: Colors.grey, width: 0.5),
          borderRadius: BorderRadius.circular(15)),
      height: 40,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Center(
            child: Text(
          title,
          style: const TextStyle(
            fontWeight: FontWeight.w400,
          ),
        )),
      ),
    );
  }
}
