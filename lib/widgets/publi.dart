import 'package:flutter/material.dart';

class Publi extends StatelessWidget {
  const Publi({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 370,
        decoration: BoxDecoration(
          color: 
          Colors.white,
          borderRadius: BorderRadius.circular(20.0),
        ),
        child: ClipRect(
        clipBehavior: Clip.antiAlias,
          child: Image.network(
            "https://www.farmaciasportuguesas.pt/media/wysiwyg/bannerhp_mobile.png",
            fit: BoxFit.cover,
          ),
        )
      ),
    );
  }
}