import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../Models/user_model.dart';
import '../Tile/drawer_tile.dart';

class BottonMenu extends StatelessWidget {
  final PageController pagecontroller;
  const BottonMenu({super.key, required this.pagecontroller});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return Stack(
      children: [
        Positioned(
            bottom: 0,
            child: Container(
              color: Colors.grey[50],
              width: screenWidth,
              height: 70,
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: ScopedModelDescendant<UsersModel>(
                    builder: ((context, child, model) {
                      return Row(
                        children: [
                          DrawerTile(
                            icon: Icons.home,
                            text: "Inicio",
                            controller: pagecontroller,
                            page: 0,
                          ),
                          const Spacer(),
                          DrawerTile(
                            icon: Icons.list,
                            text: "Categorias",
                            controller: pagecontroller,
                            page: 2,
                          ),
                          const Spacer(),
                          DrawerTile(
                            icon: Icons.location_on_outlined,
                            text: "Farmácias",
                            controller: pagecontroller,
                            page: 3,
                          ),
                          const Spacer(),
                          DrawerTile(
                            icon: Icons.playlist_add_check,
                            text: "Pedidos",
                            controller: pagecontroller,
                            page: 4,
                          ),
                          const Spacer(),
                          !model.isLogin()
                              ? DrawerTile(
                                  icon: Icons.person_2_outlined,
                                  text: "Meu Perfil",
                                  controller: pagecontroller,
                                  page: 1,
                                )
                              : DrawerTile(
                                  foto: model.currentUser != null
                                      ? model.currentUser?.photoURL
                                      : "",
                                  text: model.currentUser != null
                                      ? model.currentUser!.displayName
                                      : "Meu Perfil",
                                  controller: pagecontroller,
                                  page: 1,
                                ),
                        ],
                      );
                    }),
                  ),
                ),
              ),
            ))
      ],
    );
  }
}
