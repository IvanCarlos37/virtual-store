import 'package:flutter/material.dart';

class PubliSlider extends StatelessWidget {
  const PubliSlider({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 370,
      decoration: BoxDecoration(
        
        borderRadius: BorderRadius.circular(50.0),
      ),
      child: Image.network(
        "https://www.farmaciasportuguesas.pt/media/scandiweb/slider/5/0/500x500_setembro_cartao_cta_copy.png",
        fit: BoxFit.cover,
      ),
    );
  }
}
