import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:virtual_store/widgets/publi_widget.dart';

import '../widgets/bottom_menu.dart';

import '../widgets/publi.dart';
import '../widgets/result_card.dart';

class HomeTab extends StatelessWidget {
  final PageController pagecontroller;
  const HomeTab({super.key, required this.pagecontroller});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          toolbarHeight: 100.0,
          elevation: 0.0,
          flexibleSpace: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24.0),
            child: Row(
              children: [
                SizedBox(
                  width: 150,
                  height: 150,
                    child: Image.asset(
                  'assets/img/farm_online.jpg',
                )),
                const Spacer(),
                const Icon(
                  Icons.search,
                  size: 25,
                  color: Colors.grey,
                ),
                const SizedBox(
                  width: 10,
                ),
                const Icon(
                  Icons.shopping_cart_outlined,
                  size: 25,
                  color: Colors.grey,
                ),
              ],
            ),
          ),
          backgroundColor: Colors.white,
        ),
        body: FutureBuilder(
          future: FirebaseFirestore.instance.collection("Produtos").get(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return Stack(children: [
                ListView(
                  children: [
                    const PubliSlider(),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        const Padding(
                          padding: EdgeInsets.all(16.0),
                          child: Text(
                            "Tendencias do momento",
                            style: TextStyle(fontSize: 17, color: Colors.green),
                          ),
                        ),
                        FutureBuilder(
                          future: FirebaseFirestore.instance
                              .collection("Produtos")
                              .doc("Uso Médico")
                              .collection("items")
                              .get(),
                          builder: ((context, snapshot) {
                            if (!snapshot.hasData) {
                              return const Center(
                                child: CircularProgressIndicator(),
                              );
                            } else {
                              return SizedBox(
                                height: 350,
                                child: ListView.builder(
                                  itemCount: 2,
                                  itemBuilder: (context, index) {
                                    return ResultCard(
                                        snapshot: snapshot.data!.docs[index]);
                                  },
                                  scrollDirection: Axis.horizontal,
                                ),
                              );
                            }
                          }),
                        ),
                      ],
                    ),
                    const Publi(),
                    FutureBuilder(
                      future: FirebaseFirestore.instance
                          .collection("Produtos")
                          .doc("Uso Médico")
                          .collection("items")
                          .get(),
                      builder: ((context, snapshot) {
                        if (!snapshot.hasData) {
                          return const Center(
                            child: CircularProgressIndicator(),
                          );
                        } else {
                          return SizedBox(
                            height: 350,
                            child: ListView.builder(
                              itemCount: 5,
                              itemBuilder: (context, index) {
                                return ResultCard(
                                    snapshot: snapshot.data!.docs[index]);
                              },
                              scrollDirection: Axis.horizontal,
                            ),
                          );
                        }
                      }),
                    ),
                    const SizedBox(
                      height: 100,
                    )
                  ],
                ),
                BottonMenu(pagecontroller: pagecontroller)
              ]);
            }
          },
        ),
      ),
    );
  }
}
