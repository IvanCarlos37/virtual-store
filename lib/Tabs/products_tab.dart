import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:virtual_store/Tile/category_tile.dart';

import '../widgets/bottom_menu.dart';

class ProductsTab extends StatelessWidget {
  final PageController pagecontroller;
  const ProductsTab({super.key,required this.pagecontroller});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children:[
        FutureBuilder<QuerySnapshot>(
          future: FirebaseFirestore.instance.collection("Produtos").get(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return const Center(child: CircularProgressIndicator());
            } else {
             var devidetiles = ListTile.divideTiles(tiles: snapshot.data!.docs.map((doc) {
                return CategoryTile(snapshot: doc);
              }),color: Colors.grey[500]).toList();
              return ListView(
                  children:devidetiles );
            }
          }),
          BottonMenu(pagecontroller: pagecontroller)
      ] 
    );
  }
}
