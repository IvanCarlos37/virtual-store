import 'package:flutter/material.dart';
import 'package:virtual_store/Data/product_data.dart';

import '../HomeScreen/prduct_screen.dart';

class ProductTile extends StatelessWidget {
  const ProductTile({super.key, required this.type, required this.product});
  final String type;
  final ProductData product;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => Productscreen(
                  product: product,
                )));
      },
      child: Card(
         shape: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(20.0), 
    side:const  BorderSide(color: Colors.transparent, ), 
  ),
        elevation: 1,
        child: type == "gride"
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  AspectRatio(
                    aspectRatio: 1.0,
                    child: Image.network(
                      product.image?[0] ?? "",
                        fit: BoxFit.cover,
                    ),
                  ),
                  Expanded(
                      child: Container(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 30.0),
                          child: Text(
                            "Stop Piolhos Loção Anti-Parasitária Cabelos Longos 100ml c/ Pente",
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            style: TextStyle(
                                fontWeight: FontWeight.w400,
                                color: Colors.grey[700]),
                          ),
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        Text(
                          "500 CVE",
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              color: Colors.green[600],
                              fontSize: 20),
                        ),
                      ],
                    ),
                  ))
                ],
              )
            : Column(
                children: [
                  Image.network(
                    product.image?[0] ?? "",
                    fit: BoxFit.cover,
                    height: 220,
                  ),
                  Container(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          product.title ?? "",
                          style: const TextStyle(
                              fontWeight: FontWeight.w500, fontSize: 18),
                        ),
                        Text(
                          "${product.price?.toStringAsFixed(2)} CVE",
                          style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.bold,
                              fontSize: 18),
                        )
                      ],
                    ),
                  ),
                ],
              ),
      ),
    );
  }
}
