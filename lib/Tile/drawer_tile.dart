import 'package:flutter/material.dart';

class DrawerTile extends StatefulWidget {
  const DrawerTile(
      {super.key,
      this.foto,
      this.icon,
      this.text,
      required this.controller,
      required this.page});
  final IconData? icon;
  final String? text;
  final PageController controller;
  final int page;
  final String? foto;

  @override
  State<DrawerTile> createState() => _DrawerTileState();
}

class _DrawerTileState extends State<DrawerTile> {
  bool isBuilt = false;
  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() {
        isBuilt = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () {
          widget.controller.jumpToPage(widget.page);
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: SizedBox(
            height: 55,
            child: Column(
              children: [
                widget.icon != null
                    ? Icon(
                        widget.icon,
                        size: 22,
                        color: isBuilt &&
                                widget.controller.page?.round() == widget.page
                            ? Theme.of(context).primaryColor
                            : Colors.grey[700],
                      )
                    : Flexible(
                        flex: 3,
                        child: 
                             CircleAvatar(
                                backgroundImage: NetworkImage(widget.foto ??
                                    "https://st.depositphotos.com/2218212/2938/i/450/depositphotos_29387653-stock-photo-facebook-profile.jpg"),
                              )
                           
                      ),
                widget.icon != null
                    ? const SizedBox(
                        height: 8,
                      )
                    : const SizedBox(
                        height: 2,
                      ),
                Flexible(
                  flex: 2,
                  child: Text(
                    widget.text ?? "",
                    style: TextStyle(
                      fontSize: 13,
                      color: isBuilt &&
                              widget.controller.page?.round() == widget.page
                          ? Theme.of(context).primaryColor
                          : Colors.grey[700],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
