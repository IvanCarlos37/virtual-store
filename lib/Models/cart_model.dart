import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:virtual_store/Data/cart_produt.dart';

import 'package:virtual_store/Models/user_model.dart';

class CartModel extends Model {
  UsersModel? user;
  List products = [];
  CartModel(this.user);
  void addcartitem(CartProduct cartProduct) {
    products.add(cartProduct);
    FirebaseFirestore.instance
        .collection("user")
        .doc(user?.currentUser?.uid ?? "")
        .collection("cart")
        .add(cartProduct.toMap())
        .then((doc) {
      return cartProduct.cid = doc.id;
    });
    notifyListeners();
  }

  void removecartitem(CartProduct cartProduct) {
   
    FirebaseFirestore.instance
        .collection("user")
        .doc(user?.currentUser?.uid ?? "")
        .collection("cart")
        .doc(cartProduct.cid)
        .delete();
         products.remove(cartProduct);
         notifyListeners();

  }
}
