import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:flutter/material.dart';
import 'dart:async';

class UsersModel extends Model {
  @override
  void addListener(VoidCallback listener) {
    super.addListener(listener);

    _loadCurrentUser();
  }

  final FirebaseAuth _auth = FirebaseAuth.instance;
  UserCredential? firebaseuser;
  Map<String, dynamic> userdata = {};
  User? currentUser;
  bool isLoading = false;

  void signup(
      {required Map<String, dynamic> userdata,
      required String pass,
      required VoidCallback onsucess,
      required Function(String) onfailled}) {
    isLoading = true;

    notifyListeners();
    _auth
        .createUserWithEmailAndPassword(
            email: userdata["email"], password: pass)
        .then((user) {
      _saveUserData(userdata);

      firebaseuser = user;
      isLoading = false;
      notifyListeners();
      onsucess();
    }).catchError((e) {
      isLoading = false;
      notifyListeners();
      onfailled(e.message);
    });
  }

  Future googleSigin() async {
    final GoogleSignInAccount? googlesignin = await GoogleSignIn().signIn();
    final GoogleSignInAuthentication googleuser =
        await googlesignin!.authentication;

    final credential = GoogleAuthProvider.credential(
        idToken: googleuser.idToken, accessToken: googleuser.accessToken);
    final UserCredential authResult =
        await FirebaseAuth.instance.signInWithCredential(credential);
    currentUser = authResult.user!;
print(currentUser);
    notifyListeners();
  }

  void sigin(
      {required String email,
      required String pass,
      required VoidCallback onsucess,
      required Function(String) onfailled}) async {
    isLoading = true;

    notifyListeners();
    _auth
        .signInWithEmailAndPassword(email: email, password: pass)
        .then((user) async {
      firebaseuser = user;
      await _loadCurrentUser();
      isLoading = false;
      onsucess();
      notifyListeners();
    }).catchError((e) {
      isLoading = false;
      onfailled(e.message);
      notifyListeners();
    });
  }

  void recoverPass(String email) {
    _auth.sendPasswordResetEmail(email: email);
  }

  bool isLogin() {
    return firebaseuser != null || currentUser != null;
  }

  Future<void> _saveUserData(Map<String, dynamic> userdata) async {
    this.userdata = userdata;
    await FirebaseFirestore.instance
        .collection("users")
        .doc(firebaseuser?.user?.uid)
        .set(userdata);
  }

  void signout() async {
    await _auth.signOut();
    userdata = {};
    firebaseuser = null;
    currentUser = null;
    notifyListeners();
  }

  Future _loadCurrentUser() async {
    if (firebaseuser == null) {
    } else {
      if (userdata["nome"] == null) {
        DocumentSnapshot docUser = await FirebaseFirestore.instance
            .collection("users")
            .doc(firebaseuser?.user?.uid)
            .get();
        userdata = docUser.data() as Map<String, dynamic>;
      }
    }
    notifyListeners();
  }
}
