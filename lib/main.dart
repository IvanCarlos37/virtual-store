import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:virtual_store/Models/cart_model.dart';
import 'package:virtual_store/Models/user_model.dart';

import 'HomeScreen/home_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ScopedModel<UsersModel>(
      model: UsersModel(),
      child: ScopedModelDescendant(
        builder: (context, child,UsersModel model) {
          return ScopedModel<CartModel>(
            model: CartModel(model),
            child: MaterialApp(
                debugShowCheckedModeBanner: false,
                title: 'Flutter Demo',
                theme: ThemeData(primaryColor: Colors.green),
                home: HomeScreen()),
          );
        },
      ),
    );
  }
}
