import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:virtual_store/Data/product_data.dart';

class CartProduct {
  String? uid;
  String? cid;
  String? pid;
  String? category;
  int quantity = 0;
  ProductData? productData;

  CartProduct.fromdcument(DocumentSnapshot document) {
    Map<String, dynamic> snaposhotdocument =
        document.data() as Map<String, dynamic>;
    cid = document.id;
    category = snaposhotdocument["category"];
    quantity = snaposhotdocument["quantity"];
    pid = snaposhotdocument["pid"];
  }
  Map<String, dynamic> toMap() {
    return {
      "category": category,
      "quantity": quantity,
      "product": productData != null ? productData!.toresume() : null
    };
  }
}
