import 'package:cloud_firestore/cloud_firestore.dart';

class ProductData {
  String? category;
  String? id;
  String? title;
  String? description;
  double? price;
  List? image;
  ProductData.fromdocument(DocumentSnapshot snapshot) {
    var data = snapshot.data() as Map<String, dynamic>;
    id = snapshot.id;
    title = data["title"];
    description = data["description"];
    price = data["price"] + 0.0;
    image = data["image"];
  }

  Map<String, dynamic> toresume() {
    return {"description": description, "title": title, "price": price};
  }
}
